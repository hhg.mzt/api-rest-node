let express = require('express')
let http = require('http')
let app = express()

let users = ['Hiram', 'Irving', 'Chava', 'Jesus', 'Ivan', 'Tachala']

app.get('/', (req, res)=> {
    res.status(200).send("Welcome to API REST")
})

app.get('/users', (req, res) => {
    res.send(users)
})

app.post('/users', (req, res) => {
    users.push('User ' + users.length)
    res.send("New user add")
})

app.patch('/users', (req, res) => {
    res.send("PATC method")
})

app.delete('/users', (req, res) => {
    res.send("DELETE method")
})

app.get('/users/oscar/avatar', (req, res) => {
    console.log(res);
    res.send('Hello GET:/users/oscar/avatar')
})

app.put('/users/oscar/avatar', (req, res) => {
    res.send('Hello PUT:/users/oscar/avatar')
})

app.delete('/users/oscar', (req, res) => {
    res.send('Hello DELETE:/users/oscar')
})

app.post('/users', (req, res) => {
    res.send('Hello POST:/users')
})

app.get('/cources', (req, res) => {
    let coupon = req.query.coupon
    let source = req.query.source
    res.send("Coupo: " + coupon + ", Source: " + source)
})


app.get('/clients', function (req, res) {

    let sql = require("mssql");
    // Configuration object for your database
    let config = {
        user: 'sa',
        password: '******',
        server: '****',
        database: '*****'
    };
    // connect to the database
    sql.connect(config, function (err) {
        if (err) console.log(err);
        // create Request object
        let request = new sql.Request();
        // query to the database and get the records
        request.query('SELECT * FROM cat_clientes WHERE activo = 1', function (err, recordset) {
            if (err) console.log(err)
            // send records as a response
            res.send(recordset);

        });
    });
});

app.get('/hello', (req, res) => {
    res.status(200).send({"message": "Hello User"})
})

http.createServer(app).listen(8001, () => {
    console.log('Server started at http://localhost:8001');
})